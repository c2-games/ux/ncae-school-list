#!/usr/bin/env python3

import csv
import json
import os
import shutil
from collections import namedtuple

os.makedirs("output", exist_ok=True)

schools = []
with open("schools.csv", "r") as f:
    reader = csv.reader(f)
    headers = next(reader)

    School = namedtuple("School", headers)
    for row in reader:
        schools.append(School(*row))


with open("output/schools-v1.json", "w") as f:
    f.write(json.dumps([s._asdict() for s in schools], sort_keys=True, indent=2))

shutil.copyfile("schools.csv", "output/schools-v1.csv")
